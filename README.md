# README #

#Description
This is common gradle tasks to help with building microservices. 
You can find some documentation of this  in the [wiki](https://bibliocommons.atlassian.net/wiki/display/ARC/Build+System)
 
 #Changelog
 ###1.1.1 
 Bug related to the failure of `gradle clean check` caused by deletion of
 build directory after gradle configuration fixed.
 ###1.1.2
 Publishing sources
